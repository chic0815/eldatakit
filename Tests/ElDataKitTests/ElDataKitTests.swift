import XCTest
@testable import ElDataKit

final class ElDataKitTests: XCTestCase {
    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct
        // results.
        XCTAssertEqual(ElDataKit().text, "Hello, World!")
    }

    static var allTests = [
        ("testExample", testExample),
    ]
}
