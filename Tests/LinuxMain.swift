import XCTest

import ElDataKitTests

var tests = [XCTestCaseEntry]()
tests += ElDataKitTests.allTests()
XCTMain(tests)
