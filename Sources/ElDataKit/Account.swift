//
//  File.swift
//  
//
//  Created by 이재성 on 2019/12/22.
//

import Foundation

public class ElData {
    public static func signIn(identifier: String?, username: String, useremail: String) {
        print("[ElData] \(#function) has been invoked")
        
        let identifier = identifier ?? UUID().uuidString
        self.save(identifier: identifier, username: username, useremail: useremail)
        self.autoSignInOption(isOn: true)
    }
    
    public static func signOut() {
        print("[ElData] \(#function) has been invoked")
        let userDefault = UserDefaults.standard
        userDefault.set(nil, forKey: "ElDataKit_account_identifier")
        userDefault.set(nil, forKey: "ElDataKit_account_username")
        userDefault.set(nil, forKey: "ElDataKit_account_useremail")
        
        self.autoSignInOption(isOn: false)
    }
    
    public static func isRequiredAuth() -> Bool {
        let userDefault = UserDefaults.standard
        guard let isAuto = userDefault.value(forKey: "ElDataKit_account_auto_sign_in") as? Bool else { return false }
        return isAuto
    }
    
    public static func fetchData() -> (identifier: String, name: String, email: String)? {
        print("[ElData] \(#function) has been invoked")
        
        let userDefault = UserDefaults.standard
        
        guard let identifier = userDefault.value(forKey: "ElDataKit_account_identifier") as? String else { return nil }
        guard let username = userDefault.value(forKey: "ElDataKit_account_username") as? String else { return nil }
        guard let useremail = userDefault.value(forKey: "ElDataKit_account_useremail") as? String else { return nil }
        
        return (identifier, username, useremail)
    }
    
    private static func save(identifier: String, username: String, useremail: String) {
        print("[ElData] \(#function) has been invoked")
        
        let userDefault = UserDefaults.standard
        
        userDefault.set(identifier, forKey: "ElDataKit_account_identifier")
        userDefault.set(username, forKey: "ElDataKit_account_username")
        userDefault.set(useremail, forKey: "ElDataKit_account_useremail")
    }
    
    private static func autoSignInOption(isOn: Bool) {
        print("[ElData] \(#function) has been invoked")
        
        let userDefault = UserDefaults.standard
        userDefault.set(isOn, forKey: "ElDataKit_account_auto_sign_in")
    }
}
